<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name'];

    public function pacient() {
        return $this->hasOne('App\Pacient');
    }
}
