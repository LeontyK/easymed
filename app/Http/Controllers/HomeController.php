<?php

namespace App\Http\Controllers;

use App\Pacient;
use Illuminate\Http\Request;
use App\Branch;

class HomeController extends Controller
{
    public function index() {
        $branches = Branch::all();
        $pacients = Pacient::all();

        return view('home', [
            'branches' => $branches,
            'pacients' => $pacients
        ]);
    }
}
