<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacient extends Model
{
    protected $fillable = ['surname', 'name', 'patronymic', 'birth', 'address', 'diagnosis', 'note', 'branch_id'];

    public function branch() {
        return $this->belongsTo('App\Branch');
    }
}
