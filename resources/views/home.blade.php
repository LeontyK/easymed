<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/main.css')}}">

    <title>EasyMED</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#pacientAdd">
        <span data-feather="plus"></span>
        Добавить
    </button>
    @include('modal.pacients.add')
    @include('modal.branches.add')

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Отделения
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @if(isset($branches))
                        @foreach($branches as $branch)
                            <a class="dropdown-item" href="#">{{$branch->name}}</a>
                        @endforeach
                        <div class="dropdown-divider"></div>
                    @endif
                    <a class="dropdown-item" href="" data-toggle="modal" data-target="#branchAdd">
                        Добавить
                    </a>
                </div>
            </li>
            <li class="nav-item">

            </li>
        </ul>
        <input type="text" class="form-control" placeholder="Поиск" title="ФИО ДР Диагноз" id="search" autofocus oninput="search(this.value)">
    </div>
</nav>

@if(!count($pacients) == 0)
    <table class="table table-striped">
        <thead>
            <tr class="tab-header">
                <th scope="col">Фамилия</th>
                <th scope="col">Имя</th>
                <th scope="col">Отчество</th>
                <th scope="col">Дата рождения</th>
                <th scope="col">Адрес</th>
                <th scope="col">Диагноз</th>
                <th scope="col">Отделение</th>
                <th scope="col">Примечание</th>
                <th scope="col" class="text-center">Действие</th>
            </tr>
        </thead>
        <tbody id="defaultTable">
            @foreach($pacients as $pacient)
                @include('modal.pacients.edit')
                @include('modal.pacients.delete')
                <tr id="pac{{$pacient->id}}">
                    <td>{{$pacient->surname}}</td>
                    <td>{{$pacient->name}}</td>
                    <td>{{$pacient->patronymic}}</td>
                    <td>{{date('d.m.Y', strtotime($pacient->birth))}}</td>
                    <td>{{$pacient->address}}</td>
                    <td>{{$pacient->diagnosis}}</td>
                    <td>
                        @if(isset($pacient->branch))
                            {{$pacient->branch->name}}
                        @endif
                    </td>
                    <td>{{$pacient->note}}</td>
                    <td class="text-center">
                        <a class="nav-link active action-link" href="" title="Редактировать" data-toggle="modal" data-target="#pacientEdit{{$pacient->id}}">
                            <span data-feather="edit"></span>
                        </a>
                        <a class="nav-link active action-link" href="" title="Удалить" data-toggle="modal" data-target="#pacientDel{{$pacient->id}}">
                            <span data-feather="delete"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <div class="container h-100">
        <h1 class="text-center p-5">Нет пациентов</h1>
    </div>
@endif

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('js/jquery-3.3.1.slim.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Icons -->
<script src="{{asset('js/feather.min.js')}}"></script>
<script>
    feather.replace()
</script>

<script>
    function search(text) {

        var defTab;
        defTab = $("#defaultTable");

        if (text.length > 2) {
            clearSearch();
            var keywords = text.split(' ');

            defTab.addClass('hidetr');

            var pacients = {!! json_encode($pacients) !!}

            pacients.forEach(function(item, i, arr) {
                var copy_item = Object.assign({}, item);
                var search_array = [];
                var success;

                for (var k in copy_item) {
                    if (k == 'created_at' || k == 'updated_at'  || k == 'id' || k == 'branch_id' || k == 'branch' || k == 'address' || k == 'note' || copy_item[k] == null) {
                        delete copy_item[k];
                    }
                }

                keywords.forEach(function (keyword) {
                    var re = new RegExp(keyword, 'i');
                    var result;

                    for(var key in copy_item) {

                        if (keywords.length > 1) {
                            var full_match = new RegExp('^'+keyword+'$', 'i');

                            if (keyword !== keywords[keywords.length - 1]) {
                                result = copy_item[key].search(full_match);
                                if (result == 0) {
                                    search_array[search_array.length] = result;
                                    delete copy_item[key];
                                }
                            }
                            else {
                                if (keywords.length-1 == search_array.length) {
                                    result = copy_item[key].match(re);
                                    if (result) {
                                        success = 1;
                                    }
                                }
                            }
                        }
                        else {
                            result = copy_item[key].match(re);
                            if (result) {
                                success = 1;
                            }
                        }
                    }
                });

                if (success == 1) {
                    var my_not_null = function (param) {
                        if (param !== null) {
                            return param.name;
                        }
                    };

                    $('#pac'+item.id+'').addClass('showtr');
                }
            });
        }
        else {
            resetTable()
        }
    }
    function clearSearch() {
        $(".showtr").removeClass('showtr');
    }
    function resetTable() {
        clearSearch();

        var defTab;
        defTab = $("#defaultTable");

        defTab.removeClass('hidetr');
    }
</script>
</body>
</html>