<div class="modal fade" id="pacientEdit{{$pacient->id}}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редактирование пациента</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('pacients.update', $pacient)}}" method="post">
                <input type="hidden" name="_method" value="PUT">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputSurname">Фамилия</label>
                            <input type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" id="inputSurname" name="surname" placeholder="Фамилия" value="{{$pacient->surname or ""}}" required>
                            @if ($errors->has('surname'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputName">Имя</label>
                            <input type="text" class="form-control" id="inputName" placeholder="Имя" name="name" value="{{$pacient->name or ""}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPatronymic">Отчество</label>
                            <input type="text" class="form-control" id="inputPatronymic" placeholder="Отчество" name="patronymic" value="{{$pacient->patronymic or ""}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputDate">Дата рождения</label>
                            <input type="date" class="form-control" id="inputDate" max="{{date('d/m/Y')}}" name="birth" value="{{$pacient->birth or ""}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Адрес</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="Город, улица, дом, квартира" name="address" value="{{$pacient->address or ""}}">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputDiagnosis">Диагноз</label>
                            <input type="text" class="form-control" id="inputDiagnosis" placeholder="Диагноз" name="diagnosis" value="{{$pacient->diagnosis or ""}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputBranch">Отделение</label>
                            <select id="inputBranch" class="form-control" @if(!isset($branches)) disabled @endif name="branch_id">
                                <option></option>
                                @foreach($branches as $branch)
                                    <option value="{{$branch->id}}" @if($pacient->branch_id == $branch->id) selected @endif>{{$branch->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputNote">Примечание</label>
                        <input type="text" class="form-control" id="inputNote" placeholder="Дополнительная информация" name="note" value="{{$pacient->note or ""}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>
